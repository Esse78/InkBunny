﻿using Microsoft.UI.Xaml.Controls;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace InkBunny
{
    public partial class MainPage : ContentPage
    {
        private int page = 1;
        private int pageMax;
        private int maxRow = 6;
        private int maxColumn = 5;
        public ObservableCollection<string> searchElements { get; set; }

        public MainPage()
        {
            InitializeComponent();
            
            searchElements = new ObservableCollection<string>();
            searchElementList.ItemsSource = searchElements;
            searchElementList.ItemTapped += remove_SearchElement;

            var url = "https://raw.githubusercontent.com/LearnWebCode/json-example/master/pets-data.json";
            pageMax = 10;

            DownloadJsonAsync(url);

            BindingContext = this;
        }

        private void OnBackClicked(object sender, EventArgs e)
        {   
            if (page == 1)
            {
                return;
            }

            page--;

        }

        private void OnPlayClicked(object sender, EventArgs e)
        {
        }

        private void OnDownloadClicked(object sender, EventArgs e)
        {
        }

        private void OnNextClicked(object sender, EventArgs e)
        {
            if (page == pageMax)
            {
                return;
            }
            page++;

        }

        private void ricerca(object sender, EventArgs e)
        {
            var enteredText = ((Entry)sender).Text;
            searchElements.Add(enteredText);
            ((Entry)sender).Text = "";
        }

        private void remove_SearchElement(object sender, ItemTappedEventArgs e)
        {
            // Retrieve the tapped item
            var tappedItem = (string)e.Item;

            // Remove the tapped item from the ObservableCollection
            searchElements.Remove(tappedItem);
        }

        private async void imageClick(object sender, EventArgs e)
        {
            try
            {
                var imageSource = ((ImageButton)sender).Source;
                var stringPath = imageSource.ToString().Substring(5);
                // Instantiate the custom pop-up page
                var popupPage = new ImagePopup();

                // Set the image source
                popupPage.SetImageSource(stringPath);

                // Display the pop-up
                await Navigation.PushModalAsync(popupPage);
            }
            catch ( Exception ex ){
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }


        public async Task DownloadImageAsync(string imageUrl, string outputPath)
        {
            try
            {
                // Create a new HttpClient instance
                using (var httpClient = new HttpClient())
                {
                    // Send a GET request to the URL
                    HttpResponseMessage response = await httpClient.GetAsync(imageUrl);

                    // Check if the request was successful
                    if (response.IsSuccessStatusCode)
                    {
                        // Read the content of the response as a byte array
                        byte[] imageBytes = await response.Content.ReadAsByteArrayAsync();

                        // Write the byte array to a file
                        File.WriteAllBytes(outputPath, imageBytes);
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine($"Failed to download image. Status code: {response.StatusCode}");
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"Error downloading image: {ex.Message}");
            }
        }

        public async void DownloadJsonAsync(string url)
        {
            string varName;
            int i;
            try
            {
                // Create a new HttpClient instance
                using (var httpClient = new HttpClient())
                {
                    // Send a GET request to the URL
                    
                    HttpResponseMessage response = await httpClient.GetAsync(url);

                    // Check if the request was successful
                    if (response.IsSuccessStatusCode)
                    {
                        // Deserialize the JSON response into a list of Person objects
                        imageJson jsonItems = await response.Content.ReadFromJsonAsync<imageJson>();
                        i = 0;
                        foreach (var pet in jsonItems.pets)
                        {
                            varName = "Image" + i / maxColumn + i % maxRow;
                            var ImageButton = Images.FindByName<ImageButton>(varName);
                            ImageButton.Source = pet.photo;
                            i++;
                        }
                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine($"Failed to download JSON. Status code: {response.StatusCode}");
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine($"Error downloading JSON: {ex.Message}");
            }
        }

    }
}


public class imageJson
{
    public image[] pets { get; set; }
}
public class image
{
    public string name { get; set; }
    public string species { get; set; }
    public string[] favFoods { get; set; }
    public int birthYear { get; set; }
    public string photo { get; set; }
}