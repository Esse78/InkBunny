using Microsoft.Maui.Controls;

namespace InkBunny
{
    public partial class ImagePopup : ContentPage
    {
        public ImagePopup()
        {
            InitializeComponent();
        }

        // Method to set the image source
        public void SetImageSource(string imagePath)
        {
            popupImage.Source = imagePath;
        }

        // Event handler for closing the pop-up
        private async void OnCloseButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }
    }
}